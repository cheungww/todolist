import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {addTodo, searchTodo} from '../actions.js';

class AddTodo extends Component{
  constructor(props, context){
    super(props, context); 

    this.onSubmit = this.onSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);

    this.state = {
      value: ''
    };
  }

  onInputChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  onSubmit(ev){
    ev.preventDefault();
    console.log(document.activeElement.name);
    
    const inputValue = this.state.value;
    if (!inputValue.trim()) {
     return;
    }

    if (document.activeElement.name === "add_btn"){
      this.props.onAdd(inputValue);
      this.setState({value: ''});
    }    
    else {
      console.log('search');
      this.props.onSearch(inputValue);
    }
  }
  
  render(){
    return(
      <div className="add-todo">
        <form onSubmit={this.onSubmit}>
          <input className="new-todo" onChange={this.onInputChange} value={this.state.value} />
          <button className="search-btn" name="search_btn" type="submit" >
            搜索
          </button>
          <button className="add-btn" name="add_btn" type="submit" id="test" >
            添加
          </button>    
        </form>
      </div>
    );
  }
}

AddTodo.propTypes = {
  onAdd: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAdd: (text) => {
      dispatch(addTodo(text));
    },

    onSearch: (text) => {
      dispatch(searchTodo(text));
    }
  };
};

export default connect(null, mapDispatchToProps)(AddTodo);