import {ADD_TODO, TOGGLE_TODO, REMOVE_TODO, SEARCH_TODO} from './actionTypes.js';

export default (state = [], action) => {
  var lists = [];
  switch(action.type){ 
    case SEARCH_TODO: {
      state.forEach((onelist) => {
        if (onelist.text.indexOf(action.text) === -1){
          return;
        }

        lists.push(onelist);
      })

      
      return lists;
    }
    case ADD_TODO: {
      return [
        {
          id: action.id,
          text: action.text,
          completed: false
        },
        ...state
      ]  
    }

    case TOGGLE_TODO: {
      console.log('enter reducer TOGGLE_TODO' + action.id)
      return state.map((todoItem) => {
        if(todoItem.id === action.id){
          return {...todoItem, completed: !todoItem.completed};
        }
        else{
          return todoItem;
        }
      })
    }

    case REMOVE_TODO: {
      console.log('enter reducer REMOVE_TODO' + action.id)
      return state.filter((todoItem) => {
        return todoItem.id !== action.id;
      })
    }

    default: 
      return state;
  }
}